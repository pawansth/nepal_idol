<?php

namespace app\googlApi;

use google\apiclient; 

class validationConf {

    public function validate($packName, $prodId, $purchaseToken) {
        try {
    // create new Google Client
    $client = new \Google_Client();
    // set Application Name to the name of the mobile app
    // get p12 key file
    $key = file_get_contents('android-developer.p12');
    // create assertion credentials class and pass in:
    // - service account email address
    // - query scope as an array (which APIs the call will access)
    // - the contents of the key file
    $cred = new \Google_Auth_AssertionCredentials(
        'idoapi@api-7260402299773817403-221587.iam.gserviceaccount.com',
        ['https://www.googleapis.com/auth/androidpublisher'],
        $key
    );
    
    // add the credentials to the client
    $client->setAssertionCredentials($cred);
 
    // create a new Android Publisher service class
    $service = new \Google_Service_AndroidPublisher($client);
    // set the package name and subscription id
    $packageName = $packName;
    $productId = $prodId;
 
    // use the purchase token to make a call to Google to get the subscription info
    $production = $service->purchases_products->get($packageName, $prodId, $purchaseToken);
    if(!empty($production['purchaseTimeMillis'])) {
        return true;
    }
} catch (\Exception $e) {
    return false;
    // if the call to Google fails, throw an exception
    // throw new Exception('Error validating transaction', 500);
}
    }

}

?>