<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use app\models\LoginForm;

use app\googlApi\validationConf;


Yii::$app->request->enableCsrfValidation = false;

class SiteController extends Controller {

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays singerdetail.
     *
     * @return string
     */
    public function actionSingers($id = null) {
        // $id = Yii::$app->request->get('id');
        if ($id == NULL) {
                 $singerModel = \app\models\SingerInfo::find()->andFilterWhere(['id' => $id])->asArray()->all();
          } else {
                 $singerModel = \app\models\SingerInfo::find()->andFilterWhere(['id' => $id])->asArray()->one();
         }
        return json_encode($singerModel);
    }

    /**
     * Displays singerdetail.
     *
     * @return string
     */
    public function actionPurchase($token) {
        $userModel = $this->findModel($token);
        $response = ['message' => 'Invalid token', 'flag' => FALSE];
        if ($userModel) {
            $token = Yii::$app->request->get('token_id');
            $votes = Yii::$app->request->get('votes');
            $amount = Yii::$app->request->get('amount');
            $package_name=Yii::$app->request->get('package_name');
            $product_id=Yii::$app->request->get('product_id');
            $purchase_time=Yii::$app->request->get('purchase_time');
            //validate token code
            $validate = new validationConf();
            $valid =  $validate->validate($package_name, $product_id, $token);
            if ($valid) {
                $model = new \app\models\Purchase();
                $model->fk_user_id = $userModel->id;
                $model->vote = $votes;
                $model->amount = $amount;
                $model->product_id = $product_id;
                $model->package_name = $package_name;
                $model->token = $token;
                $model->create_date = $purchase_time;
                if (!$model->save()) {
                    $response = ['message' => 'Unable to process your request.', 'flag' => false];
                } else {
                    $response = ['message' => 'Purchase success', 'flag' => true];
                }
            }
        }
        return json_encode($response);
    }

    /**
     * Displays singerdetail.
     *
     * @return string
     */
    public function actionVote($token) {
        $userModel = $this->findModel($token);
        $response = ['message' => 'Invalid token', 'flag' => false];
        if ($userModel) {
            $vote = Yii::$app->request->get('vote');
            $singer_id = Yii::$app->request->get('singer_id');
            $totalVotes = $userModel->getPurchases()->select(['sum(vote) as vote'])->one();
            $voteUsed = $userModel->getVotes()->select(['sum(vote) as vote'])->one();
            $availableVotes = $totalVotes['vote'] - $voteUsed['vote'];
            if ($availableVotes >= $vote) {
                $voteModel = new \app\models\Vote();
                $voteModel->vote = $vote;
                $voteModel->fk_singer_id = (int) $singer_id;
                $voteModel->fk_user_id = (int) $userModel->id;
                $voteModel->create_date = time();
                $singer_name = $voteModel->getFkSinger()->one()->name;
                if ($voteModel->save()) {
                    $response = ['message' => "You have successfully vote($vote) for " . $singer_name, 'flag' => true];
                } else {
                    $response = ['message' => 'Unable to process your request.', 'flag' => false];
                }
            } else {
                $response = ['message' => "You don't have enough vote.", 'flag' => false];
            }
        }
        return json_encode($response);
    }

    public function actionCurrentVotes($token) {
        $userModel = $this->findModel($token);
        $response = ['message' => 'Invalid token', 'flag' => false];
        if ($userModel) {
            $totalVotes = $userModel->getPurchases()->select(['count(vote) as vote'])->one();
            $voteUsed = $userModel->getVotes()->select(['count(vote) as vote'])->one();
            $availableVotes = $totalVotes['vote'] - $voteUsed['vote'];
            $response = ['availableVotes' => $availableVotes];
        }
        return json_encode($response);
    }

    public function actionSignup() {
        $customer = new \app\models\Customer();
        $customer->name = Yii::$app->request->get('name');
        $customer->email = Yii::$app->request->get('email');
        $customer->password = Yii::$app->request->get('password');
        $response = ['message' => 'User detail', 'flag' => false];
        $token = $customer->generateUniqueRandomString(200);
        if ($token) {
            $customer->token = $token;
            if ($customer->save()) {
                $response = ['message' => 'Signup success.', 'flag' => true];
            }
        } else {
            $response = ['message' => 'Unable to process your request.', 'flag' => false];
        }

        return json_encode($response);
    }

    public function actionVoteHistory($token) {
        $userModel = $this->findModel($token);
        $response = ['message' => 'Invalid token', 'flag' => false];
        if ($userModel) {
            $voteHistory = $userModel->getVotes()->with('fkSinger')->all();
            $response=[];
            foreach ($voteHistory as $vh) {
                $response[] = ['singer_name' => $vh->fkSinger->name, 'vote' => $vh->vote, 'date' => date('d M, Y ', $vh->create_date)];
            }
        }
        return json_encode($response);
    }

    public function actionPurchaseHistory($token) {
        $userModel = $this->findModel($token);
        $response = ['message' => 'Invalid token', 'flag' => false];
        if ($userModel) {
            $purchaseHistory = $userModel->getPurchases()->all();
            $response=[];
            foreach ($purchaseHistory as $ph) {
                $response[] = ['amount'=>$ph->amount,'vote' => $ph->vote, 'date' => date('d M, Y ', $ph->create_date)];
            }
        }
        return json_encode($response);
    }


    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin() {
        $model = new LoginForm();
        $response = ['message' => 'Invalid email or password','flag'=>false];
        $model->username=Yii::$app->request->get('username');
        $model->password=Yii::$app->request->get('password');
        if ($model->login()) {
            $response = ['message' => 'Login success.','flag'=>true ,'username'=>Yii::$app->user->identity->name,'email'=>Yii::$app->user->identity->email,'token' => Yii::$app->user->identity->token];
            Yii::$app->user->logout();
        }
        return json_encode($response);
    }

    protected function findModel($token) {
        if (($model = \app\models\Customer::findOne(['token' => $token])) !== null) {
            return $model;
        } else {
            return false;
        }
    }

}
