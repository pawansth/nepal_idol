<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vote".
 *
 * @property integer $id
 * @property integer $fk_user_id
 * @property integer $fk_singer_id
 * @property integer $vote
 * @property integer $create_date
 *
 * @property User $fkUser
 * @property SingerInfo $fkSinger
 */
class Vote extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vote';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fk_user_id', 'fk_singer_id', 'vote', 'create_date'], 'required'],
            [['fk_user_id', 'fk_singer_id', 'vote', 'create_date'], 'integer'],
            [['fk_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['fk_user_id' => 'id']],
            [['fk_singer_id'], 'exist', 'skipOnError' => true, 'targetClass' => SingerInfo::className(), 'targetAttribute' => ['fk_singer_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fk_user_id' => 'Fk User ID',
            'fk_singer_id' => 'Fk Singer ID',
            'vote' => 'Vote',
            'create_date' => 'Create Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'fk_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkSinger()
    {
        return $this->hasOne(SingerInfo::className(), ['id' => 'fk_singer_id']);
    }
}
