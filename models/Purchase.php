<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "purchase".
 *
 * @property integer $id
 * @property integer $fk_user_id
 * @property string $amount
 * @property string $vote
 * @property string $package_name
 * @property string $product_id
 * @property string $token
 * @property integer $create_date
 * @property integer $pdate_time
 *
 * @property User $fkUser
 */
class Purchase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'purchase';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fk_user_id', 'amount', 'vote', 'package_name', 'product_id', 'token', 'create_date'], 'required'],
            [['fk_user_id', 'create_date', 'pdate_time'], 'integer'],
            [['amount'], 'string', 'max' => 10],
            [['vote'], 'string', 'max' => 50],
            [['package_name', 'product_id', 'token'], 'string', 'max' => 255],
            [['fk_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['fk_user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fk_user_id' => 'Fk User ID',
            'amount' => 'Amount',
            'vote' => 'Vote',
            'package_name' => 'Package Name',
            'product_id' => 'Product ID',
            'token' => 'token ID',
            'create_date' => 'Create Date',
            'pdate_time' => 'Pdate Time',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'fk_user_id']);
    }
}
