<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "singer_info".
 *
 * @property integer $id
 * @property string $name
 * @property string $detail
 * @property integer $position
 * @property string $image
 * @property integer $status
 *
 * @property Vote[] $votes
 */
class SingerInfo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'singer_info';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'detail', 'position'], 'required'],
            [['detail'], 'string'],
            [['position', 'status'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['image'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'detail' => 'Detail',
            'position' => 'Position',
            'image' => 'Image',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVotes()
    {
        return $this->hasMany(Vote::className(), ['fk_singer_id' => 'id']);
    }
}
