<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $token
 *
 * @property Purchase[] $purchases
 * @property Vote[] $votes
 */
class Customer extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name', 'email', 'password'], 'required'],
            [['name', 'email', 'password'], 'string', 'max' => 255],
            [['token'], 'string', 'max' => 50],
            [['accessToken', 'authKey'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'password' => 'Password',
            'token' => 'Token',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurchases() {
        return $this->hasMany(Purchase::className(), ['fk_user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVotes() {
        return $this->hasMany(Vote::className(), ['fk_user_id' => 'id']);
    }

    public function generateUniqueRandomString($try = 100) {
        $try--;
        $randomString = 'NEPID.' . Yii::$app->getSecurity()->generateRandomString(31);
        if (!$this->findOne(["token" => $randomString])) {
            return $randomString;
        } elseif ($try > 0) {
            return $this->generateUniqueRandomString($try);
        } else {
            return false;
        }
    }

    public function getId() {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey() {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey) {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password) {
        return $this->password === $password;
    }

    public static function findIdentity($id) {
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null) {
        return $this->accessToken;
    }

    public static function findbyUsername($uname) {
        return self::find()->where(['email' => $uname])->one();
    }

}
